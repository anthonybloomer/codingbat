def string_times(str, n):
    return str * n


def front_times(str, n):
    front = 3
    if front > len(str):
        front = len(str)
    front = str[:front]
    result = ""
    for i in range(n):
        result = result + front
    return result


def sting_bits(str):
    result = ""
    for i in range(len(str)):
        if i % 2 == 0:
            result = result + str[i]
        return result


def string_splosion(str):
    result = ""
    for i in range(len(str)):
        result = result + str[:i + 1]
    return result


def last2(str):
    count = 0
    for i in range(len(str) - 2):
        sub = str[i:i + 2]
        if sub == str[-2:]:
            count += 1

    return count


def array_count9(nums):
    count = 0
    for num in nums:
        if num == 9:
            count += 1
    return count


def array_front9(nums):
    end = len(nums)
    if end > 4:
        end = 4

    for i in range(end):
        if nums[i] == 9:
            return True
    return False


def array123(nums):
    for i in range(len(nums) - 2):
        if nums[i] == 1 and nums[i + 1] == 2 and nums[i + 2] == 3:
            return True
    return False


def string_match(a, b):
    shorter = min(len(a), len(b))
    count = 0

    for i in range(shorter - 1):
        a_sub = a[i:i + 2]
        b_sub = b[i:i + 2]
        if a_sub == b_sub:
            count+= 1

    return count
