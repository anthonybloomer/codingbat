def make_bricks(small, big, goal):
    if goal % 5 > small:
        return False
    else:
        return small + big * 5 >= goal


def lone_sum(a, b, c):
    if a == b and b == c:
        return 0
    elif a == c:
        return b
    elif b == c:
        return a
    elif a == b:
        return c
    else:
        return a + b + c


def lucky_sum(a, b, c):
    if a == 13:
        return 0
    elif b == 13:
        return a
    elif c == 13:
        return a + b
    else:
        return a + b + c


def no_teen_sum(a, b, c):
    return fix_teen(a) + fix_teen(b) + fix_teen(c)


def fix_teen(n):
    if 13 <= n <= 19:
        if n == 15 or n == 16:
            return n
        else:
            return 0
    else:
        return n
