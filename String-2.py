def double_char(str):
    dc = ""
    for n in str:
        dc += n + n
    return dc


def count_hi(str):
    count = 0
    for i in range(len(str) - 1):
        if str[i] == 'h' and str[i + 1] == 'i':
            count += 1
    return count


def cat_dog(str):
    cc = 0
    dc = 0
    for i in range(len(str) - 2):
        if str[i:i+3]=='cat':
            cc+=1
        if str[i:i+3]=='dog':
            dc+=1
    return cc == dc


def count_code(str):
    count = 0
    for i in range(len(str) - 3):
        if str[i] == 'c' and str[i + 1] == 'o' and str[i + 3] == 'e':
            count += 1
    return count


def end_other(a, b):
    a = a.lower()
    b = b.lower()
    return a.endswith(b) or b.endswith(a)


def xyz_there(str):
    str = str.replace('.xyz', '')
    return "xyz" in str
