def count_evens(nums):
    return len([num for num in nums if num % 2 == 0])


def big_diff(nums):
    return abs(max(nums) - min(nums))

def centered_average(nums):
  nums = sorted(nums)
  nums = nums[1:-1]
  return sum(nums) / len(nums)

def sum13(nums):
    sum = 0
    index = False
    if len(nums) == 0:
        return 0
    for n in nums:
        if index:
            index = False
        else:
            if n == 13:
                index = True
            else:
                sum += n
                index = False
    return sum


def sum67(nums):
    sum = 0
    skip = False
    for num in nums:
        if num == 6:
            skip = True
            continue
        if num == 7:
            if not skip:
                sum += num
            else:
                skip = False
            continue
        if not skip:
            sum += num
    return sum


def has22(nums):
    for i in range(len(nums) - 1):
        if nums[i] == 2 and nums[i + 1] == 2:
            return True

    return False
